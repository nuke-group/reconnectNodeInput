# Reconnect Node Input  

Reconnects dot & PostageStamp nodes' input(s) when copy/pasting

Tested with Nuke: _11.2v1, 11.3v4, 12.1v5, 12.2v6, 13.0v1_

![](/doc/reconnectNodes_demo.gif)

## Installation

- Copy the *reconnectNodeInput* folder to your _~/.nuke_ folder
- Add this to your *init.py*:

    ```python
    import os
    import nuke
    nuke.pluginAddPath(os.path.join(os.path.dirname(__file__), "reconnectNodeInput"))
    ```

- Done. When starting Nuke, the code inside `reconnectNodeInput/menu.py` sets everything up for you

## Preferences

In the Nuke Preferences you can change the shortcut and which node classes should be reconnected when pasted:

![](/doc/reconnectNodeInput_preferences.png)

## How to use

- `Ctrl+C` or `Ctrl+X` to copy/cut nodes as usual
- `Ctrl+V` to paste & reconnect them
- `Ctrl+Shift+V` to paste normally without reconnecting
