"""
Reconnects the previous input of the pasted node
"""
import re
import nuke
import nukescripts
from preferencesManager import preferencesManager

#----------------------------------------
# VARIABLES
#----------------------------------------
VERSION = "1.1.2"
BASENAME = "reconnectNodeInput"
PARENTNODEKNOBBASENAME = "%s_parentNode_"%(BASENAME)
PARENTCOMPKNOBNAME = "%s_parentComp"%(BASENAME)
PREFERENCES = None

#----------------------------------------
# CLASSES
#----------------------------------------
class Preferences(preferencesManager.Preferences):
    """Manages reconnectNodeInput's Preferences
    """

    def __init__(self):
        creatorName = "Fynn Laue"
        appName = "Reconnect Node Input"
        self.pasteShortcut = "Ctrl+V"
        super(Preferences, self).__init__(creatorName, appName)

    def setup(self):
        """Add knobs to the Preferences and define defaults
        """
        # Remove old knobs
        self.removeKnob("knobClasses")
        # Enable
        knob = nuke.Boolean_Knob("enable", "Enable")
        knob.setTooltip("Uncheck this if you want to disable reconnecting inputs.")
        knob.setValue(True)
        knob.setFlag(nuke.STARTLINE)
        self.addKnob(knob, defaultValue=True)
        # Node Classes
        knob = nuke.String_Knob("nodeClasses", "Nodes")
        knob.setTooltip("Comma separated list of node classes, that should reconnect to their previous input when pasted.\nTo get the class of a node:\n- Select the node\n- Press i\n- Name of the class is defined at 'Class=<name>'")
        knob.setValue("Dot, PostageStamp")
        if knob.getFlag(nuke.STARTLINE):
            knob.clearFlag(nuke.STARTLINE)
        self.addKnob(knob, defaultValue="Dot, PostageStamp")
        # Paste Shortcut
        knob = nuke.String_Knob("pasteShortcut", "Shortcut")
        knob.setTooltip("Define a shortcut for pasting and reconnecting the previously copied/cut nodes.\nFor example: Ctrl+Alt+Shift+V\nIf you accidentally override something, just set the shortcut to what you actually wanted, restart Nuke and everything will be ok.")
        knob.setFlag(nuke.STARTLINE)
        self.addKnob(knob, defaultValue=self.pasteShortcut)

#----------------------------------------
# FUNCTIONS
#----------------------------------------
# BACKEND
def getValidNodeClasses():
    """Check Preferences for which node classes to reconnect
    :return: [description]
    :rtype: [type]
    """
    knob = PREFERENCES.getKnob('nodeClasses')
    result = []
    if knob:
        nodeClassList = knob.value().split(",")
        result = [i.strip(" ") for i in nodeClassList]
    return result

def registerNodeParent(node):
    """Registers the specified node's input node and the name of the script they're in.
    :param nodes: Node which' input(s) to register
    :type nodes: nuke.Node
    """
    # Register all connected inputs' parentNodes
    for inputIndex in range(node.inputs()):
        # Create knob to register parentNode if it doesn't exit yet
        parentNodeKnobName = "%s%s"%(PARENTNODEKNOBBASENAME, inputIndex)
        if not node.knob(parentNodeKnobName):
            parentNodeKnob = nuke.String_Knob(parentNodeKnobName, "parentNode_%s"%(inputIndex))
            node.addKnob(parentNodeKnob)
        parentNodeKnob = node.knob(parentNodeKnobName)
        parentNodeKnob.setFlag(nuke.INVISIBLE)
        # Register input if a node is connnected, other wise clear field
        inputNode = node.input(inputIndex)
        if inputNode:
            parentNodeKnob.setValue(inputNode.name())
        else:
            parentNodeKnob.setValue("")
    # Remove knobs for disconnected inputs
    for inputIndex in range(node.maxInputs()):
        parentNodeKnobName = "%s%s"%(PARENTNODEKNOBBASENAME, inputIndex)
        knob = node.knob(parentNodeKnobName)
        if knob:
            inputNode = node.input(inputIndex)
            if inputNode is None:
                node.removeKnob(knob)
    # Create knob to register parentComp
    if not node.knob(PARENTCOMPKNOBNAME):
        parentCompKnob = nuke.String_Knob(PARENTCOMPKNOBNAME, "parentComp")
        node.addKnob(parentCompKnob)
    parentCompKnob = node.knob(PARENTCOMPKNOBNAME)
    parentCompKnob.setFlag(nuke.INVISIBLE)
    parentCompKnob.setValue(nuke.root().name())
    # Hide 'User' knob if no other user-created knobs are visible or exist
    pastUserKnob = False
    knobsVisible = []
    for knob in node.allKnobs():
        if pastUserKnob:
            knobsVisible.append(knob.getFlag(nuke.INVISIBLE))
        if knob.name() == "User":
            pastUserKnob = True
    userTabKnob = node.knob("User")
    if userTabKnob:
        if knobsVisible and all(knobsVisible):
            userTabKnob.setFlag(nuke.INVISIBLE)
        else:
            userTabKnob.clearFlag(nuke.INVISIBLE)

def reconnectNode(node):
    """Reconnects the node's input(s) to the ones registered previously
    :param node: Node to reconnect
    :type node: nuke.Node
    :return: On success: True, on Failure: False
    :rtype: bool
    """
    # Check if parentComp matches current comp
    currentComp = nuke.root().name()
    parentCompKnob = node.knob(PARENTCOMPKNOBNAME)
    if parentCompKnob:
        if currentComp == parentCompKnob.value():
            inputIndex = 0
            # Loop through all knobs and check name to not skip any non-sequencial inputs.
            regExp = re.compile(r"(?<=%s)[0-9]+$"%(PARENTNODEKNOBBASENAME))
            for knob in node.allKnobs():
                if regExp.search(knob.name()):
                    inputIndex = int(regExp.search(knob.name()).group(0))
                    parentNode = nuke.toNode(knob.value())
                    if parentNode:
                        # Don't override input if it is already connected.
                        if not node.input(inputIndex):
                            node.setInput(inputIndex, parentNode)
            return True
        else:
            return False

# MAIN FUNCTIONALITY
def copyNodesWithParent():
    """Registers parentNode(s) and parentScript on selected nodes (if their Class is in NODECLASSES list) and copy them to the clipboard.
    """
    if PREFERENCES.getKnob("enable").value():
        selectedNodes = nuke.selectedNodes()
        validNodeClasses = getValidNodeClasses()
        if selectedNodes:
            for node in selectedNodes:
                if node.Class() in validNodeClasses:
                    registerNodeParent(node)
    # Copy node(s) to clipboard
    nuke.nodeCopy(nukescripts.edit.cut_paste_file())

def cutNodesWithParent():
    """Same as copyNodesWithParent, except copied nodes are deleted afterward
    """
    copyNodesWithParent()
    nuke.nodeDelete(True)

def pasteNodesWithParent():
    """Pastes nodes and reconnects their input(s)
    """
    nuke.nodePaste(nukescripts.edit.cut_paste_file())
    if PREFERENCES.getKnob("enable").value():
        selectedNodes = nuke.selectedNodes()
        validNodeClasses = getValidNodeClasses()
        if selectedNodes:
            for node in selectedNodes:
                if node.Class() in validNodeClasses:
                    reconnectNode(node)

# HELPERS
def getHotkeyDict(invert=False):
    """Get dictionary of all assigned hotkeys

    :param invert: If True: {hotkey : command}, if False: {command: hotkey}, defaults to False
    :param invert: bool, optional
    :return: dictionary of hotkeys and commands
    :rtype: dict
    """
    hotkeys = nuke.hotkeys()
    hotkeyDict = {}
    for entry in hotkeys.split("\n"):
        keyVal = entry.split("\t")
        if len(keyVal) > 1:
            key = keyVal[0].lstrip(" ")
            val = keyVal[1].strip(" ")
            if val != "":
                if invert:
                    hotkeyDict[val] = key
                else:
                    hotkeyDict[key] = val
    return hotkeyDict

def preferencesChanged(knob):
    """Update the current shortcut for pasting and reconnecting if it has changed
    """
    if knob is PREFERENCES.getKnob("pasteShortcut"):
        hotkeyDict = getHotkeyDict(invert=True)
        if knob.value().lower() == PREFERENCES.pasteShortcut.lower():
            pass
        elif knob.value().lower() in [i.lower() for i in hotkeyDict.keys()]:
            if nuke.ask("%s is already assigned to '%s'. Are you sure you want to reassign it?"%(knob.value(), hotkeyDict.get(knob.value()))):
                setupShortcuts()
                PREFERENCES.pasteShortcut = knob.value()
            else:
                PREFERENCES.blockSignals(True)
                knob.setValue(PREFERENCES.pasteShortcut)
                PREFERENCES.blockSignals(False)

# SETUP
def setupShortcuts():
    """Add shortcuts for registering parent nodes and reconnecting them
    Per default overrides cut/copy and paste
    """
    editMenu = nuke.menu("Nuke").menu("Edit")
    editMenu.addCommand("Copy", copyNodesWithParent, shortcut="Ctrl+C")
    editMenu.addCommand("Cut", cutNodesWithParent, shortcut="Ctrl+X")
    pasteShortcut = PREFERENCES.getKnob("pasteShortcut").value()
    editMenu.addCommand("Paste and reconnect", pasteNodesWithParent, shortcut=pasteShortcut, shortcutContext=2)

def setup():
    """Set up preferences and shortcuts and connect Signals
    """
    global PREFERENCES
    PREFERENCES = Preferences()
    PREFERENCES.setup()
    setupShortcuts()
    PREFERENCES.changed.connect(preferencesChanged)
